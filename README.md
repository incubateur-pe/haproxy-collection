# Ansible Collection - incubateurpe.haproxy

Various roles to install and configure haproxy

## incubateurpe.haproxy.install

Installs haproxy and optionnaly create an example configuration ( without configuration, haproxy will be unable to start )

__example:__

```
- hosts: all
  roles:
    - role: incubateurpe.haproxy.install
```


## incubateurpe.haproxy.tcp_proxy

Configures a tcp_proxy frontend and backend and reload haproxy

__example:__

```
- hosts: all
  roles:
    - role: incubateurpe.haproxy.tcp_proxy
      vars:
        haproxy_bind_addresses:
          - 127.0.0.1:8888

```


