incubateurpe.haproxy.install
=========

Installs haproxy and optionnaly sets up an example configuration serving a single static page


Role Variables
--------------

haproxy_allow_bind_non_local_ip: true
haproxy_connect_timeout: 5000
haproxy_client_timeout: 50000
haproxy_server_timeout: 50000


Example Playbook
----------------

```
    - hosts: servers
      roles:
         - role: incubateurpe.haproxy.install
```

License
-------

BSD-3-Clause

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
