"""Role testing files using testinfra."""
import pytest


def test_haproxy_installed(host):
    haproxy = host.package("haproxy")
    assert haproxy.is_installed


def test_config_file(host):
    f = host.file("/etc/haproxy/haproxy.cfg")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"


@pytest.mark.parametrize("directory", [
    "conf.d",
    "errorfiles"
])
def test_configuration_directories(host, directory):
    f = host.file("/etc/haproxy/{0}".format(directory))

    assert f.exists
    assert f.is_directory
    assert f.mode == 0o755
    assert f.user == "root"
    assert f.group == "root"


def test_haproxy_running_and_enabled(host):
    haproxy = host.service("haproxy")
    assert haproxy.is_running
    assert haproxy.is_enabled


def test_haproxy_stats_is_listening(host):
    socket = host.socket("tcp://0.0.0.0:8404")

    assert socket.is_listening
