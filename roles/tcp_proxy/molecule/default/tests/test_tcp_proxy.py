"""Role testing files using testinfra."""


def test_config_file(host):
    f = host.file("/etc/haproxy/conf.d/example.cfg")

    assert f.exists
    assert f.user == "root"
    assert f.group == "root"


def test_haproxy_running(host):
    haproxy = host.service("haproxy")
    assert haproxy.is_running


def test_haproxy_example_is_listening(host):
    socket = host.socket("tcp://127.0.0.1:8888")

    assert socket.is_listening
